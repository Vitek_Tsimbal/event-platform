const Neo4JDB = require('neo4j-schema');
const db = new Neo4JDB({
    uri: 'bolt://localhost:7687',
    username: 'neo4j',
    password: 'neo4jdb'
}).connect()

module.exports = {
  db
}