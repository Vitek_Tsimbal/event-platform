module.exports = {
  model: {
    title: String,
    createdAt: {type: Date, default: Date.now}
  }
}

