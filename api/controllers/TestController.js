const placeModel = require('./../models/place.js')
const create = require('./../blueprints/create.js').create

module.exports = {
  testView: function (req, res, next) {
    create(placeModel, 'n', 'place', {
      title: 'Test place'
    })
    return res.render('index', {title: 'Test title'})
  }
}