const schemaConnection = require('./../../config/db/schemaCreation.js').db
const Neo4JDB = require('neo4j-schema');

module.exports = {
  create: function (Model, variable, label, props) {
    const model = schemaConnection.model(label, Neo4JDB.Schema(Model))
    
    console.log('Create props')
    console.log(props)
    model.create({
      variable,
      label,
      props
    })
  }
}