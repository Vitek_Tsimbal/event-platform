var express = require('express');
var router = express.Router();

const testController = require('./../api/controllers/TestController.js').testView

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/test', (req, res, next) => testController(req, res, next))

module.exports = router;
